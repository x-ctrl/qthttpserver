QT += network core-private

include($$PWD/3rdparty/http-parser.pri)

HEADERS += \
    $$PWD/qtcpsslserver.h \
    $$PWD/qtcpsslserver_p.h \
    $$PWD/qthttpserverglobal.h \
    $$PWD/qabstracthttpserver.h \
    $$PWD/qabstracthttpserver_p.h \
    $$PWD/qhttpserver.h \
    $$PWD/qhttpserver_p.h \
    $$PWD/qhttpserverliterals_p.h \
    $$PWD/qhttpserverrequest.h \
    $$PWD/qhttpserverrequest_p.h \
    $$PWD/qhttpserverresponder.h \
    $$PWD/qhttpserverresponder_p.h \
    $$PWD/qhttpserverresponse.h \
    $$PWD/qhttpserverresponse_p.h \
    $$PWD/qhttpserverrouter.h \
    $$PWD/qhttpserverrouter_p.h \
    $$PWD/qhttpserverrouterrule.h \
    $$PWD/qhttpserverrouterrule_p.h \
    $$PWD/qhttpserverrouterviewtraits.h \
    $$PWD/qhttpserverviewtraits.h \
    $$PWD/qhttpserverviewtraits_impl.h

SOURCES += \
    $$PWD/qabstracthttpserver.cpp \
    $$PWD/qhttpserver.cpp \
    $$PWD/qhttpserverliterals.cpp \
    $$PWD/qhttpserverrequest.cpp \
    $$PWD/qhttpserverresponder.cpp \
    $$PWD/qhttpserverresponse.cpp \
    $$PWD/qhttpserverrouter.cpp \
    $$PWD/qhttpserverrouterrule.cpp \
    $$PWD/qtcpsslserver.cpp

    QT += concurrent
    HEADERS += $$PWD/qhttpserverfutureresponse.h
    SOURCES += $$PWD/qhttpserverfutureresponse.cpp


